DynDNSWeb
=========

A DynDNS2/HTTP to nsupdate proxy

What it does
------------

DynDNSWeb translates DynDNS2-compatible HTTP requests into bind9-compatible
nsupdate calls.

What it doesn't
---------------

DynDNSWeb does **not** handle authentication! You will need a reverse-proxy for
SSL termination anyway, so authentication (HTTP basic auth) should be done
there. DynDNSWeb only handles authorization, i.e. it checks if the authenticated
user has access to the requested hostname.

Installation / Usage
--------------------

DynDNSWeb is a Flask app, so you can run it via Flask
(`FLASK_APP=dyndnsweb.py flask run`) or uwsgi (`uwsgi --ini uwsgi.ini`).
An example systemd service and socket file (using uwsgi) as well as an example
nginx vhost can be found in the examples directory.

Dependencies
------------

DynDNSWeb requires `nsupdate`, python 3, Flask 0.12 and pexpect 4.2.

HTTP Request format
-------------------

DynDNSWeb expects the following GET parameters:
* `hostname`: The fully qualified domain name that should be updated
* `ip4`: Optional. If specified, an A record with this ip address will be set
* `ip6`: Optional. If specified, an AAAA record with this ip address will be set
* Either `ip4`, `ip6` or both must be set

For example, if DynDNSWeb runs under https://dyndns.example.net/ and you want
test.dyndns.example.net to point to 127.0.0.1, then your request URI
looks like
`https://dyndns.example.net/?hostname=test.dyndns.example.net&ip4=127.0.0.1`.  
Additionally, the username must be passed via HTTP basic auth (nginx auth_basic
does this, other webservers haven't been tested yet).

User Hostname access
--------------------

To keep things simple, usernames and hostnames are the same. This means that
the user named `bob.example.net` has access to the hostname `bob.example.net`.
Additionally, if `allow_subdomains` is set in the config file, then
`bob.example.net` also has access to every subdomain of `bob.example.net`.
