# dyndnsweb - DynDNS/HTTP to nsupdate proxy
# https://sgebauer.net/projects/dyndnsweb
# Copyright (c) 2018 Sven Gebauer
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

from flask import Flask, Response, request
import configparser
import ipaddress
import pexpect
import sys
import re

app = Flask(__name__)
configparser = configparser.ConfigParser({
  "dnsserver": "127.0.0.1",
  "zone": "dyn.example.com",
  "keyfile": "./dns.key",
  "ttl": "300",
  "allow_subdomains": "True"
})
configparser.read('config.ini')
config = configparser['config']

@app.route('/')
def update():
  username = request.authorization.username
  hostname = request.args.get('hostname')
  ip4 = request.args.get('ip4')
  ip6 = request.args.get('ip6')

  if not username:
    return Response('Missing basic auth username', status=400)
  elif not hostname:
    return Response('Missing hostname parameter', status=400)
  elif not is_valid_hostname(hostname):
    return Response('Invalid hostname', status=400)
  elif not check_hostname_access(username, hostname):
    return Response('Hostname access denied', status=400)
  elif ip4 and not is_valid_ip(ip4, 4):
    return Response('Invalid ip4 address', status=400)
  elif ip6 and not is_valid_ip(ip6, 6):
    return Response('Invalid ip6 address', status=400)
  elif ip4 == None and ip6 == None:
    return Response('No ip specified', status=400)
  else:
    update_dns(hostname, ip4, ip6)
    return Response('Update successfull', status=200)

def is_valid_hostname(hostname):
  if hostname == None or len(hostname) > 255:
    return False
  allowed = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)
  return all(allowed.match(x) for x in hostname.split("."))

def check_hostname_access(username, hostname):
  return hostname == username or (config.get('allow_subdomains')
    and hostname.endswith('.' + username))

def is_valid_ip(ip, version):
  try:
    return ipaddress.ip_address(ip).version == version
  except ValueError:
    return False

def update_dns(hostname, ip4, ip6):
  proc = pexpect.spawn('/usr/bin/nsupdate', ['-k', config.get('keyfile')],
    echo=False, encoding='utf-8')
  proc.expect('>')
  proc.sendline('server %s' % config.get('dnsserver'))
  proc.expect('>')
  proc.sendline('zone %s' % config.get('zone'))
  proc.expect('>')
  proc.sendline('update delete %s.' % hostname)
  proc.expect('>')
  if ip4 != None:
    proc.sendline('update add %s %s A %s' % (hostname, config.get('ttl'), ip4))
    proc.expect('>')
  if ip6 != None:
    proc.sendline('update add %s %s AAAA %s' %
      (hostname, config.get('ttl'), ip6))
    proc.expect('>')

  proc.sendline('send')
  proc.expect('>')
  proc.close()
  proc.wait()
